package by.shymko.composite.textmanager;

import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.exceptions.TechnicalException;
import by.shymko.composite.textcomposite.TextComposite;
import by.shymko.composite.textcomposite.component.Component;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.PrintWriter;


/**
 * Created by Andrey on 26.02.2015.
 */
public class TextWriter {
    public static void writeText(Component component, String path) throws LogicalException, TechnicalException {
        final Logger LOG = Logger.getLogger(TextWriter.class);

        try (PrintWriter pw = new PrintWriter(path)) {
            LOG.info("writing start");
            for (TextComposite composite : component.getComponents()) {
                pw.print(composite.toText());
            }
            LOG.info("writing finish");
        } catch (FileNotFoundException e) {
            throw new TechnicalException(e);
        } catch (LogicalException e) {
            throw new LogicalException(e);
        }

    }
}
