package by.shymko.composite.textmanager;

import by.shymko.composite.constants.RegularExpressions;
import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.exceptions.TechnicalException;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Andrey on 25.02.2015.
 */
public class TextReader {
    private static final Logger LOG = Logger.getLogger(TextReader.class);

    public static String read(String path) throws LogicalException, TechnicalException {
        try(FileReader reader = new FileReader(path)) {
            LOG.info("reading start");
            Scanner scanner = new Scanner(reader);
            StringBuffer str = new StringBuffer();
            String tmp;
            while ( scanner.hasNext()){
                tmp = scanner.nextLine();
                str.append(tmp).append(RegularExpressions.END_OF_LINE);
            }
            LOG.info("reading finish");
            return str.toString();
        } catch (FileNotFoundException e) {
            throw new LogicalException(e);
        } catch (IOException e) {
            throw new LogicalException(e);
        }
    }
}
