package by.shymko.composite.constants;

/**
 * Created by Andrey on 25.02.2015.
 */
public class RegularExpressions {
    public static final String LISTING_SIGN = "\\${4}";
    public static final String PARAGRAPH_SIGN = "\\ {4}";
    public static final String END_OF_LINE  = "\n";
//    public static final String DELIMETRS = "\\p{Punct}|\\ ";
    public static final String DELIMETRS = "\\ ";
    public static final String END_OF_SENTENCE = "\\.|\\!|\\?|\\n +";
    public static final String UPPER_FIRST_WORD = "\\p{Upper}|[А-Я].+";

}
