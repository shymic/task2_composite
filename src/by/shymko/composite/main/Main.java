package by.shymko.composite.main;

import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.exceptions.TechnicalException;
import by.shymko.composite.textcomposite.component.Component;
import by.shymko.composite.parser.TextParser;
import by.shymko.composite.textmanager.TextReader;
import by.shymko.composite.solver.TextSolver;
import by.shymko.composite.textmanager.TextWriter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;


/**
 * Created by Andrey on 28.02.2015.
 * 5. В каждом предложении поменять местами первое и последнее слово не меняя длины предложения
 * 18. Посчитать сколько слов в данном тексте начинается с прописной буквы
 */
public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class);

    static {
        new DOMConfigurator().doConfigure("resources\\log4j.xml", LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        try {
            String text = TextReader.read("resources\\data.txt");
            TextParser parser = new TextParser();
            Component component = parser.parseText(text);
            TextSolver.workWithSentenses(component);
            TextWriter.writeText(component, "resources\\report.txt");
        } catch (LogicalException e) {
            LOG.error(e);
            e.printStackTrace();
        } catch (TechnicalException e) {
            LOG.error(e);
        }
    }
}
