package by.shymko.composite.parser;

import by.shymko.composite.textcomposite.component.Component;
import by.shymko.composite.textcomposite.component.ComponentType;
import by.shymko.composite.constants.RegularExpressions;
import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.textcomposite.leaf.Listing;
import by.shymko.composite.textcomposite.leaf.Punctuation;
import by.shymko.composite.textcomposite.leaf.Word;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Andrey on 25.02.2015.
 */
public class TextParser {
    private static final Logger LOG = Logger.getLogger(TextParser.class);

    public Component parseText(String text) throws LogicalException {
        Pattern pattern = Pattern.compile(RegularExpressions.LISTING_SIGN);
        String[] listingCuttingText = pattern.split(text);
        Matcher matcher = pattern.matcher(text);
        ArrayList<String> parsedText = textToParsedText(listingCuttingText, matcher);
        matcher.reset();
        return separateListings(matcher, parsedText);

    }

    private ArrayList<String> textToParsedText(String[] listingCuttingText, Matcher matcher) {
        ArrayList<String> parsedText = new ArrayList<>();
        for (int i = 0; i < listingCuttingText.length; ++i) {
            parsedText.add(listingCuttingText[i]);
            if( matcher.find()) {
                parsedText.add(matcher.group());
            }
        }
        return parsedText;
    }

    private Component separateListings(Matcher matcher, ArrayList<String> parsedText) throws LogicalException {
        Component composedText = new Component(ComponentType.TEXT);
        Stack<String> listingStack = new Stack<>();
        for (String element : parsedText) {
            if (!element.isEmpty()) {
                if (matcher.find() && matcher.group().equals(element)) {
                    if (listingStack.empty()) {
                        listingStack.push(RegularExpressions.LISTING_SIGN);
                    } else {
                        listingStack.pop();
                    }
                    composedText.add(new Punctuation(matcher.group()));
                } else {
                    if (listingStack.empty()) {
                        composedText.add(parseTextWithoutListings(element));
                    } else {
                        composedText.add(new Listing(element));
                    }
                }
                matcher.reset();
            }
        }
        if (!listingStack.empty()) {
            LOG.info("Incorrect input data. The part of data was lost");
        }
        return composedText;
    }

    private Component parseTextWithoutListings(String textWithoutListings) throws LogicalException {
        Component paragraphComponent = new Component(ComponentType.TEXT);
        Pattern pattern = Pattern.compile(RegularExpressions.PARAGRAPH_SIGN);
        Matcher matcher = pattern.matcher(textWithoutListings);
        String[] paragraphs = pattern.split(textWithoutListings);
        for (String paragraph : paragraphs) {
            if (!paragraph.isEmpty()) {
                if (matcher.find()) {
                    paragraphComponent.add(new Punctuation(matcher.group()));
                }
                paragraphComponent.add(parseParagraph(paragraph));
            }
        }
        return paragraphComponent;
    }

    private Component parseParagraph(String paragraph) throws LogicalException {
        Component compositeParagraph = new Component(ComponentType.PARAGRAPH);
        Pattern pattern = Pattern.compile(RegularExpressions.END_OF_SENTENCE);
        Matcher matcher = pattern.matcher(paragraph);
        String[] sentenses = pattern.split(paragraph);
        for (String sentens : sentenses) {
            if (!sentens.isEmpty()) {
                compositeParagraph.add(parseSentence(sentens));
                if (matcher.find()) {
                    compositeParagraph.add(new Punctuation(matcher.group()));
                }
            }
        }
        while(matcher.find()){
            compositeParagraph.add(new Punctuation(matcher.group()));
        }
        return compositeParagraph;
    }

    private Component parseSentence(String sentence) throws LogicalException {
        Component compositeSentence = new Component(ComponentType.SENTENCE);
        Pattern pattern = Pattern.compile(RegularExpressions.DELIMETRS);
        Matcher matcher = pattern.matcher(sentence);
        String[] parsed = sentence.split(RegularExpressions.DELIMETRS);
        for (String element : parsed) {
            if (!element.isEmpty()) {
                compositeSentence.add(new Word(element));
                if (matcher.find()) {
                    compositeSentence.add(new Punctuation(matcher.group()));
                }
            }
        }
        return compositeSentence;
    }

}

