package by.shymko.composite.solver;

import by.shymko.composite.constants.RegularExpressions;
import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.textcomposite.TextComposite;
import by.shymko.composite.textcomposite.component.Component;
import by.shymko.composite.textcomposite.component.ComponentType;
import by.shymko.composite.textcomposite.leaf.Word;
import org.apache.log4j.Logger;


/**
 * Created by Andrey on 02.03.2015.
 */
public class TextSolver {
    private static final Logger LOG = Logger.getLogger(TextSolver.class);

    public static void workWithSentenses(Component text) throws LogicalException {
        for (TextComposite component: text.getComponents()){
            if(component.getType().equals(ComponentType.TEXT)){
                for (TextComposite paragraph: component.getComponents()){
                    if( paragraph.getType().equals(ComponentType.PARAGRAPH)){
                        for ( TextComposite sentense : paragraph.getComponents()){
                            if ( sentense.getType().equals(ComponentType.SENTENCE)) {
                                swapFirstAndLastWord((Component)sentense);
                                LOG.info("В \"" + sentense.toText() + "\" " +
                                        countFirstUpperWordInSentense((Component)sentense) + " слов начинается с большой буквы");
                            }
                        }
                    }
                }
            }
        }
    }


    public static void swapFirstAndLastWord(Component list) {
        TextComposite firstElement = list.getComponents().get(0);
        TextComposite lastElement = firstElement;
        int i = list.getComponents().size()-1;
        while( !list.getComponents().get(i).getClass().equals(Word.class)){
            i--;
        }
        lastElement = list.getComponents().get(i);
        list.getComponents().set(0, lastElement);
        list.getComponents().set(list.getComponents().size() - 1, firstElement);
    }

    public static int countFirstUpperWordInSentense(Component sentense) throws LogicalException {
        int counter = 0;
        for (TextComposite word : sentense.getComponents()) {
            if (word.toText().matches(RegularExpressions.UPPER_FIRST_WORD)) {
                counter++;
            }
        }
        return counter;
    }

}

