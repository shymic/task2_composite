package by.shymko.composite.textcomposite.leaf;

import by.shymko.composite.textcomposite.component.ComponentType;
import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.textcomposite.TextComposite;

import java.util.ArrayList;

/**
 * Created by Andrey on 25.02.2015.
 */
public class Word implements TextComposite {
    private String word;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public String toText() throws LogicalException {
        return word;
    }

    @Override
    public ArrayList<TextComposite> getComponents() throws LogicalException {
        throw new LogicalException();
    }
    @Override
    public ComponentType getType() {
        return ComponentType.LEAF;
    }

    @Override
    public boolean add(TextComposite component) {
        return false;
    }
}
