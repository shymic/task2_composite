package by.shymko.composite.textcomposite.leaf;


import by.shymko.composite.textcomposite.component.ComponentType;
import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.textcomposite.TextComposite;

import java.util.ArrayList;

/**
 * Created by Andrey on 25.02.2015.
 */
public class Listing implements TextComposite {
    private String listing;

    public Listing(String listing) {
        this.listing = listing;
    }

    public String getListing() {
        return listing;
    }

    public void setListing(String listing) {
        this.listing = listing;
    }

    @Override
    public String toText() throws LogicalException {
        return listing.toString();
    }

    @Override
    public ArrayList<TextComposite> getComponents() throws LogicalException {
        throw new LogicalException();
    }

    @Override
    public ComponentType getType() {
        return ComponentType.LISTING;
    }

    @Override
    public boolean add(TextComposite component) {
        return false;
    }
}
