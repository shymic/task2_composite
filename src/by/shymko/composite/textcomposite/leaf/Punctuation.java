package by.shymko.composite.textcomposite.leaf;

import by.shymko.composite.textcomposite.component.ComponentType;
import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.textcomposite.TextComposite;

import java.util.ArrayList;

/**
 * Created by Andrey on 25.02.2015.
 */
public class Punctuation implements TextComposite {
    private String punctuation;

    public Punctuation(String punctuation) {
        this.punctuation = punctuation;
    }

    public String getPunctuation() {
        return punctuation;
    }

    public void setPunctuation(String punctuation) {
        this.punctuation = punctuation;
    }

    @Override
    public String toText() throws LogicalException {
        return punctuation;
    }
    @Override
    public ArrayList<TextComposite> getComponents() throws LogicalException {
        throw new LogicalException();
    }
    @Override
    public ComponentType getType() {
        return ComponentType.LEAF;
    }

    @Override
    public boolean add(TextComposite component) {
        return false;
    }
}
