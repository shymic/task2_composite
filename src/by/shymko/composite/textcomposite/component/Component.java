package by.shymko.composite.textcomposite.component;

import by.shymko.composite.exceptions.LogicalException;
import by.shymko.composite.textcomposite.TextComposite;

import java.util.ArrayList;

/**
 * Created by Andrey on 25.02.2015.
 */
public class Component implements TextComposite {

    private ArrayList<TextComposite> components;
    private ComponentType type;

    public Component(ArrayList<TextComposite> components) {
        this.components = components;
    }

    public Component(ComponentType type, ArrayList<TextComposite> components) {
        this.type = type;
        this.components = components;
    }

    public Component(ComponentType type) {
        this.type = type;
        this.components = new ArrayList<TextComposite>();
    }

    public ComponentType getType() {
        return type;
    }

    public void setType(ComponentType type) {
        this.type = type;
    }

    public ArrayList<TextComposite> getComponents() {
        return components;
    }

    public void setComponents(ArrayList<TextComposite> components) {
        this.components = components;
    }

    @Override
    public String toText() throws LogicalException {
        StringBuffer stringBuffer = new StringBuffer();
        for( TextComposite component: components){
            stringBuffer.append(component.toText());
        }
        return stringBuffer.toString();
    }

    public boolean add(TextComposite t) {
        return components.add(t);
    }
}
