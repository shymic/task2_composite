package by.shymko.composite.textcomposite.component;

/**
 * Created by Andrey on 25.02.2015.
 */
public enum ComponentType {
    TEXT,
    LISTING,
    PARAGRAPH,
    SENTENCE,
    LEAF
}
