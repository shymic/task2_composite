package by.shymko.composite.textcomposite;


import by.shymko.composite.textcomposite.component.ComponentType;
import by.shymko.composite.exceptions.LogicalException;

import java.util.ArrayList;

/**
 * Created by Andrey on 25.02.2015.
 */

public interface TextComposite {
    public String toText() throws LogicalException;
    public ArrayList<TextComposite> getComponents() throws LogicalException;
    public ComponentType getType();
    public boolean add( TextComposite component);
}
